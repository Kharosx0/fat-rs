//! Integration tests for parsing a fat32 library
#![allow(unused_variables)]

extern crate fat;

use std::collections::HashMap;
use std::ffi::OsStr;
use std::{fmt, mem, str};
use std::fs::File;
use std::io::prelude::*;
use std::io::SeekFrom;
use std::path::PathBuf;
use std::process::Command;

use fat::*;

struct FileDevice {
    file: File,
}

impl fmt::Debug for FileDevice {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "FileDevice {{")?;

        let file_size = self.file.metadata().map_err(|_| fmt::Error)?.len();
        write!(f, " size: {} bytes ", file_size)?;

        write!(f, "}}")
    }
}

impl StorageDevice for FileDevice {
    fn initialize(id: u8) -> FatResult<Self> {
        let image_path = format!("test{}.img", id);
        let file = File::open(image_path).map_err(|_| FatError::IoError)?;

        Ok(FileDevice {
            file,
        })
    }

    fn status(&self, id: u8) -> FatResult<DeviceStatus> {
        Err(FatError::InvalidDeviceId)
    }

    fn read(&mut self, id: u8, buffer: &mut [u8], sector: SectorOffset) -> FatResult<()> {
        let start_byte = (Into::<u32>::into(sector) * 512) as u64;
        self.file.seek(SeekFrom::Start(start_byte)).map_err(|_| FatError::IoError)?;
        let end_byte = buffer.len();
        self.file.read(&mut buffer[..end_byte]).map_err(|_| FatError::IoError)?;
        Ok(())
    }

    fn write(&mut self, id: u8, buffer: &[u8], sector: SectorOffset) -> FatResult<()> {
        let start_byte = (Into::<u32>::into(sector) * 512) as u64;
        self.file.seek(SeekFrom::Start(start_byte)).map_err(|_| FatError::IoError)?;
        self.file.write(buffer).map_err(|_| FatError::IoError)?;
        Ok(())
    }
}

#[derive(Debug)]
enum Object {
    File(PathBuf),
    Directory,
}

/// Creates a test FAT32 disk image using the files specified in files.
///
/// `files` specifies a mapping from source paths on the target system to their destination path in
/// the image.
///
/// files is a map from file system paths in the resulting image to source objects on the current
/// host filesystem.
fn generate_image(test_image: &str, files: HashMap<PathBuf, Object>) {
    // Create a new disk image of 500MB in size
    // fallocate -l 500M test.img
    Command::new("fallocate")
        .args(&["-l", "500M", test_image])
        .output()
        .expect("Calling fallocate failed. Make sure it's properly installed.");
    // Format it to FAT32 with no MBR
    // mkfs -t fat -F 32 test.img
    Command::new("mkfs")
        .args(&["-t", "fat", "-F", "32", test_image])
        .output()
        .expect("Calling mkfs failed. Make sure it's properly installed.");
    // Copy the Cargo.toml file into the drive as /test.txt
    // mcopy -i test.img tests/single_sector.txt ::test.txt
    for (dest, object) in files {
        assert!(dest.is_absolute());
        // Skip over the root directory.
        let dirs: Vec<&OsStr> = dest.iter().skip(1).collect();

        // Create intermediate directories because `mcopy` won't do that for us
        if dirs.len() > 1 {
            let end = dirs.len() - 1;
            let mut path = PathBuf::new();
            for path_segment in dirs[..end].iter() {
                let path_segment = *path_segment;
                path.push(path_segment);
                println!("Creating directory {:?}", &path);
                Command::new("mmd")
                    .args(&["-i", test_image, path.to_str().unwrap()])
                    .output()
                    .expect("Calling mmd failed. Make sure it's properly installed.");
            }
        }

        match object {
            Object::File(source_file) => {
                let dest_file = format!("::{}", dest.to_str().unwrap());
                Command::new("mcopy")
                    .args(&["-i", test_image, source_file.to_str().unwrap(), &dest_file])
                    .output()
                    .expect("Calling mcopy failed. Make sure it's properly installed.");
            }
            Object::Directory => {
                Command::new("mmd")
                    .args(&["-i", test_image, dest.to_str().unwrap()])
                    .output()
                    .expect("Calling mmd failed. Make sure it's properly installed.");
            }
        }
    }
}

// Read the entire file one byte at a time
#[test]
fn test_single_sector_root_dir_read_onebyte() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/single_sector.txt");
    files.insert(PathBuf::from("/test.txt"), Object::File(test_file.clone()));
    let image_num = 0;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let mut truth = Vec::new();
    File::open(test_file).unwrap().read_to_end(&mut truth).unwrap();
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();
    let root = fs.root().unwrap();
    {
        let mut test_file = root.open_file("test.txt").unwrap().unwrap();
        let mut buf: [u8; 1] = unsafe { mem::uninitialized() };
        while !test_file.at_end() {
            test_file.read(&mut buf).unwrap();
        }
    }
}

// Attempt to read too many bytes from the file
#[test]
fn test_single_sector_root_dir_read_too_many() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/single_sector.txt");
    files.insert(PathBuf::from("/test.txt"), Object::File(test_file.clone()));
    let image_num = 1;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let mut truth = Vec::new();
    File::open(test_file).unwrap().read_to_end(&mut truth).unwrap();
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();
    let root = fs.root().unwrap();
    {
        let mut test_file = root.open_file("test.txt").unwrap().unwrap();
        let mut buf: [u8; 512] = unsafe { mem::uninitialized() };
        let bytes_read = test_file.read(&mut buf).unwrap();
        assert_eq!(Into::<u32>::into(bytes_read), 58u32);
        assert_eq!(buf[..58], truth[..]);
        assert!(test_file.at_end());
    }
}

// Attempt to read from the file after it's already at the end
#[test]
fn test_single_sector_root_dir_read_at_end() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/single_sector.txt");
    files.insert(PathBuf::from("/test.txt"), Object::File(test_file.clone()));
    let image_num = 2;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let mut truth = Vec::new();
    File::open(test_file).unwrap().read_to_end(&mut truth).unwrap();
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();
    let root = fs.root().unwrap();
    {
        let mut test_file = root.open_file("test.txt").unwrap().unwrap();
        let mut buf: [u8; 512] = unsafe { mem::uninitialized() };
        test_file.read(&mut buf).unwrap();
        assert!(test_file.at_end());
        let bytes_read = test_file.read(&mut buf).unwrap();
        assert_eq!(Into::<u32>::into(bytes_read), 0u32)
    }
}

// Attempt to read the whole cluster one byte at a time.
#[test]
fn test_full_cluster_root_dir_read_onebyte() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/full_cluster.txt");
    files.insert(PathBuf::from("/test.txt"), Object::File(test_file.clone()));
    let image_num = 3;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let mut truth = Vec::new();
    File::open(test_file).unwrap().read_to_end(&mut truth).unwrap();
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();
    let root = fs.root().unwrap();
    {
        let mut test_file = root.open_file("test.txt").unwrap().unwrap();
        let mut buf: [u8; 1] = unsafe { mem::uninitialized() };
        let mut byte_offset = 0usize;
        while !test_file.at_end() {
            test_file.read(&mut buf).unwrap();
            assert_eq!(truth[byte_offset], buf[0]);
            byte_offset += 1;
        }
    }
}

// Attempt to read the whole cluster all at once.
#[test]
fn test_full_cluster_root_dir_read() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/full_cluster.txt");
    files.insert(PathBuf::from("/test.txt"), Object::File(test_file.clone()));
    let image_num = 4;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let mut truth = Vec::new();
    File::open(test_file).unwrap().read_to_end(&mut truth).unwrap();
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();
    let root = fs.root().unwrap();
    {
        let mut test_file = root.open_file("test.txt").unwrap().unwrap();
        let mut buf: [u8; 4096] = unsafe { mem::uninitialized() };
        let bytes_read = test_file.read(&mut buf).unwrap();
        assert_eq!(Into::<u32>::into(bytes_read), 4096u32);
        for i in 0..4096 {
            assert_eq!(buf[i], truth[i]);
        }
        assert!(test_file.at_end());
    }
}

// Make a small (no whole intermediate sectors) aligned start, unaligned end read
#[test]
fn test_unaligned_multisector_read() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/full_cluster.txt");
    files.insert(PathBuf::from("/test.txt"), Object::File(test_file.clone()));
    let image_num = 5;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let mut truth = Vec::new();
    File::open(test_file).unwrap().read_to_end(&mut truth).unwrap();
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();
    let root = fs.root().unwrap();
    {
        let mut test_file = root.open_file("test.txt").unwrap().unwrap();
        let mut buf: [u8; 800] = unsafe { mem::uninitialized() };
        let bytes_read = test_file.read(&mut buf).unwrap();
        assert_eq!(Into::<u32>::into(bytes_read), 800u32);
        for i in 0..800 {
            //println!("{}", i);
            assert_eq!(buf[i], truth[i]);
        }
        assert!(!test_file.at_end());
    }
}

// Make a large (multiple intermediate sectors) aligned start, unaligned end read
#[test]
fn test_unaligned_multisector_read_large() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/full_cluster.txt");
    files.insert(PathBuf::from("/test.txt"), Object::File(test_file.clone()));
    let image_num = 6;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let mut truth = Vec::new();
    File::open(test_file).unwrap().read_to_end(&mut truth).unwrap();
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();
    let root = fs.root().unwrap();
    {
        let mut test_file = root.open_file("test.txt").unwrap().unwrap();
        let mut buf: [u8; 2000] = unsafe { mem::uninitialized() };
        let bytes_read = test_file.read(&mut buf).unwrap();
        assert_eq!(Into::<u32>::into(bytes_read), 2000u32);
        for i in 0..2000 {
            println!("{}", i);
            assert_eq!(buf[i], truth[i]);
        }
        assert!(!test_file.at_end());
    }
}

// Read a two-cluster file at once
#[test]
fn test_multicluster_read() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/two_clusters.txt");
    files.insert(PathBuf::from("/test.txt"), Object::File(test_file.clone()));
    let image_num = 7;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let mut truth = Vec::new();
    File::open(test_file).unwrap().read_to_end(&mut truth).unwrap();
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();
    let root = fs.root().unwrap();
    {
        let mut test_file = root.open_file("test.txt").unwrap().unwrap();
        let mut buf: [u8; 8192] = unsafe { mem::uninitialized() };
        let bytes_read = test_file.read(&mut buf).unwrap();
        assert_eq!(Into::<u32>::into(bytes_read), 8192u32);
        for i in 0..8192 {
            assert_eq!(buf[i], truth[i]);
        }
        assert!(test_file.at_end());
    }
}

// Read a 3 cluster file all at once
#[test]
fn test_manycluster_read() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/many_clusters.txt");
    files.insert(PathBuf::from("/test.txt"), Object::File(test_file.clone()));
    let image_num = 8;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let mut truth = Vec::new();
    File::open(test_file).unwrap().read_to_end(&mut truth).unwrap();
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();
    let root = fs.root().unwrap();
    {
        let mut test_file = root.open_file("test.txt").unwrap().unwrap();
        let mut buf: [u8; 12000] = unsafe { mem::uninitialized() };
        let bytes_read = test_file.read(&mut buf).unwrap();
        assert_eq!(Into::<u32>::into(bytes_read), 12000u32);
        for i in 0..12000 {
            assert_eq!(buf[i], truth[i], "Byte {} didn't match", i);
        }
        assert!(test_file.at_end());
    }
}

// Read each cluster one at a time.
#[test]
fn test_manycluster_cluster_read() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/many_clusters.txt");
    files.insert(PathBuf::from("/test.txt"), Object::File(test_file.clone()));
    let image_num = 9;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let mut truth = Vec::new();
    File::open(test_file).unwrap().read_to_end(&mut truth).unwrap();
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();
    let root = fs.root().unwrap();
    {
        let mut test_file = root.open_file("test.txt").unwrap().unwrap();
        {
            let mut buf: [u8; 4096] = unsafe { mem::uninitialized() };
            let bytes_read = test_file.read(&mut buf).unwrap();
            assert_eq!(Into::<u32>::into(bytes_read), 4096u32);
            for i in 0..4096 {
                assert_eq!(buf[i], truth[i], "Byte {} didn't match", i);
            }
            assert!(!test_file.at_end());
        }
        {
            let mut buf: [u8; 4096] = unsafe { mem::zeroed() };
            let bytes_read = test_file.read(&mut buf).unwrap();
            assert_eq!(Into::<u32>::into(bytes_read), 4096u32);
            for i in 0..4096 {
                assert_eq!(buf[i], truth[4096 + i], "Byte {} didn't match", 4096 + i);
            }
            assert!(!test_file.at_end());
        }
        {
            let mut buf: [u8; 3808] = unsafe { mem::uninitialized() };
            let bytes_read = test_file.read(&mut buf).unwrap();
            assert_eq!(Into::<u32>::into(bytes_read), 3808u32);
            for i in 0..3808 {
                assert_eq!(buf[i], truth[8192 + i], "Byte {} didn't match", 8192 + i);
            }
            assert!(test_file.at_end());
        }
    }
}

// Test seeking in a single cluster file. This is the most basic case.
#[test]
fn test_seek_basic() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/full_cluster.txt");
    files.insert(PathBuf::from("/test.txt"), Object::File(test_file.clone()));
    let image_num = 10;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let mut truth = Vec::new();
    File::open(test_file).unwrap().read_to_end(&mut truth).unwrap();
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();
    let root = fs.root().unwrap();
    // Test seeking to the end and back to the beginning of a file.
    {
        let mut test_file = root.open_file("test.txt").unwrap().unwrap();
        {
            // Now jump to the end and read a byte.
            let end = Into::<u32>::into(test_file.size()) - 1;
            test_file.seek(fat::SeekTo::AfterStart(end)).unwrap();
            let mut buf: [u8; 1] = unsafe { mem::uninitialized() };
            let bytes_read = test_file.read(&mut buf).unwrap();
            assert_eq!(Into::<u32>::into(bytes_read), 1u32);
            assert_eq!(buf[0], truth[end as usize]);
            assert!(test_file.at_end());

            // Read a byte at the beginning.
            test_file.seek(fat::SeekTo::AfterStart(0)).unwrap();
            let mut buf: [u8; 1] = unsafe { mem::uninitialized() };
            let bytes_read = test_file.read(&mut buf).unwrap();
            assert_eq!(Into::<u32>::into(bytes_read), 1u32);
            assert_eq!(buf[0], truth[0]);
            assert!(!test_file.at_end());
        }
    }
}

// Seek to a cluster later on in the file using SeekTo::AfterStart.
#[test]
fn test_seek_forward_clusters_afterstart() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/many_clusters.txt");
    files.insert(PathBuf::from("/test.txt"), Object::File(test_file.clone()));
    let image_num = 11;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let mut truth = Vec::new();
    File::open(test_file).unwrap().read_to_end(&mut truth).unwrap();
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();
    let root = fs.root().unwrap();
    let mut test_file = root.open_file("test.txt").unwrap().unwrap();
    {
        // Now jump to the end and read a byte.
        let end = Into::<u32>::into(test_file.size()) - 1;
        test_file.seek(fat::SeekTo::AfterStart(end)).unwrap();
        let mut buf: [u8; 1] = unsafe { mem::uninitialized() };
        let bytes_read = test_file.read(&mut buf).unwrap();
        assert_eq!(Into::<u32>::into(bytes_read), 1u32);
        assert_eq!(buf[0], truth[end as usize]);
        assert!(test_file.at_end());

        // Read a byte at the beginning.
        test_file.seek(fat::SeekTo::AfterStart(0)).unwrap();
        let mut buf: [u8; 1] = unsafe { mem::uninitialized() };
        let bytes_read = test_file.read(&mut buf).unwrap();
        assert_eq!(Into::<u32>::into(bytes_read), 1u32);
        assert_eq!(buf[0], truth[0]);
        assert!(!test_file.at_end());

        // And finally jump forward again to make sure jumping backwards properly
        // updated all internal state.
        test_file.seek(fat::SeekTo::AfterStart(1100)).unwrap();
        let mut buf: [u8; 1] = unsafe { mem::uninitialized() };
        let bytes_read = test_file.read(&mut buf).unwrap();
        assert_eq!(Into::<u32>::into(bytes_read), 1u32);
        assert_eq!(buf[0], truth[1100]);
        assert!(!test_file.at_end());
    }
}

// Seek to a cluster later on in the file using SeekTo::AfterCurrent.
#[test]
fn test_seek_forward_clusters_aftercurrent() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/many_clusters.txt");
    files.insert(PathBuf::from("/test.txt"), Object::File(test_file.clone()));
    let image_num = 12;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let mut truth = Vec::new();
    File::open(test_file).unwrap().read_to_end(&mut truth).unwrap();
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();
    let root = fs.root().unwrap();
    let mut test_file = root.open_file("test.txt").unwrap().unwrap();
    {
        // Now jump to the end and read a byte.
        let end = Into::<u32>::into(test_file.size()) - 1;
        test_file.seek(fat::SeekTo::AfterCurrent(end)).unwrap();
        let mut buf: [u8; 1] = unsafe { mem::uninitialized() };
        let bytes_read = test_file.read(&mut buf).unwrap();
        assert_eq!(Into::<u32>::into(bytes_read), 1u32);
        assert_eq!(buf[0], truth[end as usize]);
        assert!(test_file.at_end());

        // Read a byte at the beginning.
        test_file.seek(fat::SeekTo::AfterStart(0)).unwrap();
        let mut buf: [u8; 1] = unsafe { mem::uninitialized() };
        let bytes_read = test_file.read(&mut buf).unwrap();
        assert_eq!(Into::<u32>::into(bytes_read), 1u32);
        assert_eq!(buf[0], truth[0]);
        assert!(!test_file.at_end());

        // And finally jump forward again to make sure jumping backwards properly
        // updated all internal state.
        test_file.seek(fat::SeekTo::AfterCurrent(1099)).unwrap();
        let mut buf: [u8; 1] = unsafe { mem::uninitialized() };
        let bytes_read = test_file.read(&mut buf).unwrap();
        assert_eq!(Into::<u32>::into(bytes_read), 1u32);
        assert_eq!(buf[0], truth[1100]);
        assert!(!test_file.at_end());
    }
}

// Seek to a cluster later on in the file using SeekTo::BeforeCurrent.
#[test]
fn test_seek_forward_clusters_beforecurrent() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/many_clusters.txt");
    files.insert(PathBuf::from("/test.txt"), Object::File(test_file.clone()));
    let image_num = 13;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let mut truth = Vec::new();
    File::open(test_file).unwrap().read_to_end(&mut truth).unwrap();
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();
    let root = fs.root().unwrap();
    let mut test_file = root.open_file("test.txt").unwrap().unwrap();

    // Jump to the last byte and read it
    let end = Into::<u32>::into(test_file.size()) - 1;
    test_file.seek(fat::SeekTo::AfterCurrent(end)).unwrap();
    let mut buf: [u8; 1] = unsafe { mem::uninitialized() };
    let bytes_read = test_file.read(&mut buf).unwrap();
    assert_eq!(Into::<u32>::into(bytes_read), 1u32);
    assert_eq!(buf[0], truth[end as usize]);
    assert!(test_file.at_end());

    // Read a byte at the beginning.
    test_file.seek(fat::SeekTo::BeforeCurrent(end + 1)).unwrap();
    let mut buf: [u8; 1] = unsafe { mem::uninitialized() };
    let bytes_read = test_file.read(&mut buf).unwrap();
    assert_eq!(Into::<u32>::into(bytes_read), 1u32);
    assert_eq!(buf[0], truth[0]);
    assert!(!test_file.at_end());

    // And finally jump forward again to make sure jumping backwards properly
    // updated all internal state.
    test_file.seek(fat::SeekTo::AfterCurrent(1099)).unwrap();
    let mut buf: [u8; 1] = unsafe { mem::uninitialized() };
    let bytes_read = test_file.read(&mut buf).unwrap();
    assert_eq!(Into::<u32>::into(bytes_read), 1u32);
    assert_eq!(buf[0], truth[1100]);
    assert!(!test_file.at_end());
}

// Seek to a cluster later on in the file using SeekTo::BeforeEnd.
#[test]
fn test_seek_forward_clusters_beforeend() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/many_clusters.txt");
    files.insert(PathBuf::from("/test.txt"), Object::File(test_file.clone()));
    let image_num = 14;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let mut truth = Vec::new();
    File::open(test_file).unwrap().read_to_end(&mut truth).unwrap();
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();
    let root = fs.root().unwrap();
    let mut test_file = root.open_file("test.txt").unwrap().unwrap();

    // Jump to the last byte and read it
    let last_byte = Into::<u32>::into(test_file.size()) - 1;
    test_file.seek(fat::SeekTo::BeforeEnd(1)).unwrap();
    let mut buf: [u8; 1] = unsafe { mem::uninitialized() };
    let bytes_read = test_file.read(&mut buf).unwrap();
    assert_eq!(Into::<u32>::into(bytes_read), 1u32);
    assert_eq!(buf[0], truth[last_byte as usize]);
    assert!(test_file.at_end());

    // Read a byte at the beginning.
    test_file.seek(fat::SeekTo::BeforeEnd(last_byte + 1)).unwrap();
    let mut buf: [u8; 1] = unsafe { mem::uninitialized() };
    let bytes_read = test_file.read(&mut buf).unwrap();
    assert_eq!(Into::<u32>::into(bytes_read), 1u32);
    assert_eq!(buf[0], truth[0]);
    assert!(!test_file.at_end());

    // And finally jump forward again to make sure jumping backwards properly
    // updated all internal state.
    test_file.seek(fat::SeekTo::BeforeEnd(last_byte - 1099)).unwrap();
    let mut buf: [u8; 1] = unsafe { mem::uninitialized() };
    let bytes_read = test_file.read(&mut buf).unwrap();
    assert_eq!(Into::<u32>::into(bytes_read), 1u32);
    assert_eq!(buf[0], truth[1100]);
    assert!(!test_file.at_end());
}

// Test seeking to the end of a file
#[test]
fn test_seek_end() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/many_clusters.txt");
    files.insert(PathBuf::from("/test.txt"), Object::File(test_file.clone()));
    let image_num = 15;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let mut truth = Vec::new();
    File::open(test_file).unwrap().read_to_end(&mut truth).unwrap();
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();
    let root = fs.root().unwrap();

    // Seek to the end of a many-cluster file.
    {
        let mut test_file = root.open_file("test.txt").unwrap().unwrap();
        let file_size = test_file.size();
        {
            // Seek to the end using absolute
            test_file.seek(fat::SeekTo::AfterStart(file_size)).unwrap();
            println!("{:?}", test_file);
            let mut buf: [u8; 1] = unsafe { mem::uninitialized() };
            let result = test_file.read(&mut buf);
            assert!(result.is_ok());
            assert_eq!(result.unwrap(), 0u32);
            assert!(test_file.at_end());

            // And seek to the end using relative
            test_file.seek(fat::SeekTo::AfterStart(0)).unwrap();
            test_file.seek(fat::SeekTo::AfterCurrent(file_size)).unwrap();
            let result = test_file.read(&mut buf);
            assert!(result.is_ok());
            assert_eq!(result.unwrap(), 0u32);
            assert!(test_file.at_end());

            // Seek to the end using BeforeEnd(0)
            test_file.seek(fat::SeekTo::BeforeEnd(0)).unwrap();
            let result = test_file.read(&mut buf);
            assert!(result.is_ok());
            assert_eq!(result.unwrap(), 0u32);
            assert!(test_file.at_end());
        }
    }
}

// Test seeking past the end of a file
#[test]
fn test_seek_past_end() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/many_clusters.txt");
    files.insert(PathBuf::from("/test.txt"), Object::File(test_file.clone()));
    let image_num = 16;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let mut truth = Vec::new();
    File::open(test_file).unwrap().read_to_end(&mut truth).unwrap();
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();
    let root = fs.root().unwrap();

    let mut test_file = root.open_file("test.txt").unwrap().unwrap();
    let file_size = test_file.size();
    {
        // Seek using absolute
        assert!(test_file.seek(fat::SeekTo::AfterStart(file_size + 1)).is_err());
        // The file should still be at the start, so let's check the first read byte
        let mut buf: [u8; 1] = unsafe { mem::uninitialized() };
        let bytes_read = test_file.read(&mut buf).unwrap();
        assert_eq!(Into::<u32>::into(bytes_read), 1u32);
        assert_eq!(buf[0], truth[0]);

        // Seek using relative
        assert!(test_file.seek(fat::SeekTo::AfterStart(0)).is_ok());
        assert!(test_file.seek(fat::SeekTo::AfterCurrent(file_size + 1)).is_err());
        // The file should still be at the start, so let's check the first read byte
        let mut buf: [u8; 1] = unsafe { mem::uninitialized() };
        let bytes_read = test_file.read(&mut buf).unwrap();
        assert_eq!(Into::<u32>::into(bytes_read), 1u32);
        assert_eq!(buf[0], truth[0]);
    }
}

// Test seeking before the start of a file
#[test]
fn test_seek_before_start() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/many_clusters.txt");
    files.insert(PathBuf::from("/test.txt"), Object::File(test_file.clone()));
    let image_num = 17;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let mut truth = Vec::new();
    File::open(test_file).unwrap().read_to_end(&mut truth).unwrap();
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();
    let root = fs.root().unwrap();
    let mut test_file = root.open_file("test.txt").unwrap().unwrap();
    assert!(test_file.seek(fat::SeekTo::BeforeCurrent(1)).is_err());
    assert!(test_file.seek(fat::SeekTo::BeforeCurrent(15000)).is_err());
    // The file should still be at the start, so let's check the first read byte
    let mut buf: [u8; 1] = unsafe { mem::uninitialized() };
    let bytes_read = test_file.read(&mut buf).unwrap();
    assert_eq!(Into::<u32>::into(bytes_read), 1u32);
    assert_eq!(buf[0], truth[0]);
    let end = Into::<u32>::into(test_file.size()) - 1;
    assert!(test_file.seek(fat::SeekTo::BeforeEnd(end+2)).is_err());
    assert!(test_file.seek(fat::SeekTo::BeforeEnd(end+15000)).is_err());
}

// Seek forwards in the file using the absolute and relative methods confirming that they give
// identical results
#[test]
fn test_seek_forward_compare() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/many_clusters.txt");
    files.insert(PathBuf::from("/test.txt"), Object::File(test_file.clone()));
    let image_num = 17;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let mut truth = Vec::new();
    File::open(test_file).unwrap().read_to_end(&mut truth).unwrap();
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();
    let root = fs.root().unwrap();

    let mut test_file = root.open_file("test.txt").unwrap().unwrap();
    {
        // Seek to the end of the current sector using absolute
        test_file.seek(fat::SeekTo::AfterStart(512)).unwrap();
        let mut buf: [u8; 1] = unsafe { mem::uninitialized() };
        let bytes_read = test_file.read(&mut buf).unwrap();
        assert_eq!(Into::<u32>::into(bytes_read), 1u32);
        assert_eq!(buf[0], truth[512]);

        // And seek to the end of the current sector using relative
        test_file.seek(fat::SeekTo::AfterStart(0)).unwrap();
        test_file.seek(fat::SeekTo::AfterCurrent(512)).unwrap();
        let mut buf: [u8; 1] = unsafe { mem::uninitialized() };
        let bytes_read = test_file.read(&mut buf).unwrap();
        assert_eq!(Into::<u32>::into(bytes_read), 1u32);
        assert_eq!(buf[0], truth[512]);
    }
}

// Create directory trees and populate it with files and then confirm that they exist.
#[test]
fn test_find_files() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/many_clusters.txt");
    files.insert(PathBuf::from("/dir1/test.txt"), Object::File(test_file.clone()));
    let image_num = 18;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let mut truth = Vec::new();
    File::open(test_file).unwrap().read_to_end(&mut truth).unwrap();
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();
    let root = fs.root().unwrap();

    let dir1 = root.open_directory("dir1");
    assert!(dir1.is_ok());
    let dir1 = dir1.unwrap();
    assert!(dir1.is_some());
    let dir1 = dir1.unwrap();
    let mut test_file = dir1.open_file("test.txt").unwrap().unwrap();
    {
        // Seek to the end of the current sector using absolute
        test_file.seek(fat::SeekTo::AfterStart(512)).unwrap();
        let mut buf: [u8; 1] = unsafe { mem::uninitialized() };
        let bytes_read = test_file.read(&mut buf).unwrap();
        assert_eq!(Into::<u32>::into(bytes_read), 1u32);
        assert_eq!(buf[0], truth[512]);

        // And seek to the end of the current sector using relative
        test_file.seek(fat::SeekTo::AfterStart(0)).unwrap();
        test_file.seek(fat::SeekTo::AfterCurrent(512)).unwrap();
        let mut buf: [u8; 1] = unsafe { mem::uninitialized() };
        let bytes_read = test_file.read(&mut buf).unwrap();
        assert_eq!(Into::<u32>::into(bytes_read), 1u32);
        assert_eq!(buf[0], truth[512]);
    }
}

// Test opening too many root directories
#[test]
fn test_open_multiple_roots() {
    let image_num = 19;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, HashMap::new());
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();

    // Trying to open the root multiple times will fail.
    let first_open = fs.root().unwrap();
    let second_open = fs.root();
    assert_eq!(second_open.unwrap_err(), FatError::ObjectAlreadyOpen);
}

// Test opening the same file twice (this should fail)
#[test]
fn test_open_same_file_twice() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/many_clusters.txt");
    files.insert(PathBuf::from("/test1.txt"), Object::File(test_file.clone()));
    let image_num = 20;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();

    let root = fs.root().unwrap();

    // Try to open the same file twice. The first time should succeed and the
    // second should fail with a `TooManyOpenObjects` error.
    let first_open = root.open_file("test1.txt").unwrap().unwrap();
    let second_open = root.open_file("test1.txt");
    assert_eq!(second_open.unwrap_err(), FatError::ObjectAlreadyOpen);
}

// Test opening the same file twice (this should succeed)
#[test]
fn test_open_same_file_twice_success() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/many_clusters.txt");
    files.insert(PathBuf::from("/test1.txt"), Object::File(test_file.clone()));
    let image_num = 21;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();

    let root = fs.root().unwrap();

    // Try to open the same file twice, but with a `drop` in the middle such that it should succeed.
    {
        let first_open = root.open_file("test1.txt").unwrap().unwrap();
    }
    {
        let second_open = root.open_file("test1.txt").unwrap().unwrap();
    }
}

// Test opening the same directory twice (this should fail)
#[test]
fn test_open_same_directory_twice() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/many_clusters.txt");
    files.insert(PathBuf::from("/first_subdir"), Object::Directory);
    let image_num = 22;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();

    let root = fs.root().unwrap();

    let first_open = root.open_directory("first_subdir").unwrap().unwrap();
    let second_open = root.open_directory("first_subdir");
    assert_eq!(second_open.unwrap_err(), FatError::ObjectAlreadyOpen);
}

// Test opening the same directory twice (this should fail)
#[test]
fn test_open_same_directory_twice_success() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/many_clusters.txt");
    files.insert(PathBuf::from("/first_subdir"), Object::Directory);
    let image_num = 23;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();

    let root = fs.root().unwrap();

    // Try to open the same file twice, but with a `drop` in the middle such that it should succeed.
    {
        println!("Opening first");
        let first_open = root.open_directory("first_subdir").unwrap().unwrap();
    }
    {
        println!("Opening second");
        let second_open = root.open_directory("first_subdir").unwrap().unwrap();
    }
}

// Test opening multiple subdirectories from the root (this should pass)
#[test]
fn test_open_multiple_subdirs() {
    let mut files = HashMap::new();
    files.insert(PathBuf::from("/first_subdir"), Object::Directory);
    files.insert(PathBuf::from("/second_subdir"), Object::Directory);
    let image_num = 24;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();

    let root = fs.root().unwrap();

    let first_subdir = root.open_directory("first_subdir").unwrap().unwrap();
    let second_subdir = root.open_directory("second_subdir").unwrap().unwrap();
}

// Test opening too many subdirectories from the root (this should fail)
#[test]
fn test_open_too_many_subdirs() {
    let mut files = HashMap::new();
    files.insert(PathBuf::from("/first_subdir"), Object::Directory);
    files.insert(PathBuf::from("/second_subdir"), Object::Directory);
    files.insert(PathBuf::from("/third_subdir"), Object::Directory);
    files.insert(PathBuf::from("/fourth_subdir"), Object::Directory);
    files.insert(PathBuf::from("/fifth_subdir"), Object::Directory);
    let image_num = 25;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();

    let root = fs.root().unwrap();

    let first_open = root.open_directory("first_subdir").unwrap().unwrap();
    let second_open = root.open_directory("second_subdir").unwrap().unwrap();
    let third_open = root.open_directory("third_subdir").unwrap().unwrap();
    let fourth_open = root.open_directory("fourth_subdir");
    assert_eq!(fourth_open.unwrap_err(), FatError::TooManyOpenObjects);
}

/// Test detecting an ExFAT file system
#[test]
fn test_exfat() {
    // Create a new disk image of 50MB in size
    // fallocate -l 50M testX.img
    let image_num = 26;
    let image_name = format!("test{}.img", image_num);
    Command::new("fallocate")
        .args(&["-l", "50M", &image_name])
        .output()
        .expect("Calling fallocate failed. Make sure it's properly installed.");
    // Format it to ExFAT with no MBR
    // mkfs -t exfat testX.img
    Command::new("mkfs")
        .args(&["-t", "exfat", &image_name])
        .output()
        .expect("Calling mkfs failed. Make sure it's properly installed.");
    let err = FatFileSystem::<FileDevice>::mount(image_num).unwrap_err();
    assert_eq!(err, FatError::UnsupportedFileSystem(FileSystem::ExFAT))
}

/// Test detecting a FAT12 file system
#[test]
fn test_fat12() {
    // Create a new disk image of 50MB in size
    // fallocate -l 50M testX.img
    let image_num = 27;
    let image_name = format!("test{}.img", image_num);
    Command::new("fallocate")
        .args(&["-l", "50M", &image_name])
        .output()
        .expect("Calling fallocate failed. Make sure it's properly installed.");
    // Format it to FAT12 with no MBR
    // mkfs -t fat -F 12 testX.img
    Command::new("mkfs")
        .args(&["-t", "fat", "-F", "12", &image_name])
        .output()
        .expect("Calling mkfs failed. Make sure it's properly installed.");
    let err = FatFileSystem::<FileDevice>::mount(image_num).unwrap_err();
    assert_eq!(err, FatError::UnsupportedFileSystem(FileSystem::FAT12))
}

/// Test detecting a fat16 file system
#[test]
fn test_fat16() {
    // Create a new disk image of 50MB in size
    // fallocate -l 50M testX.img
    let image_num = 28;
    let image_name = format!("test{}.img", image_num);
    Command::new("fallocate")
        .args(&["-l", "50M", &image_name])
        .output()
        .expect("Calling fallocate failed. Make sure it's properly installed.");
    // Format it to FAT16 with no MBR
    // mkfs -t fat -F 16 testX.img
    Command::new("mkfs")
        .args(&["-t", "fat", "-F", "16", &image_name])
        .output()
        .expect("Calling mkfs failed. Make sure it's properly installed.");
    let err = FatFileSystem::<FileDevice>::mount(image_num).unwrap_err();
    assert_eq!(err, FatError::UnsupportedFileSystem(FileSystem::FAT16))
}

// Test reading a file that's 1.x sectors big into a bigger buffer
#[test]
fn test_one_plus_sector_large_buffer_read() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/one_plus_sectors.txt");
    files.insert(PathBuf::from("/test.txt"), Object::File(test_file.clone()));
    let image_num = 29;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let mut truth = Vec::new();
    File::open(test_file).unwrap().read_to_end(&mut truth).unwrap();
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();
    let root = fs.root().unwrap();
    {
        let mut test_file = root.open_file("test.txt").unwrap().unwrap();
        let mut buf: [u8; 800] = unsafe { mem::uninitialized() };
        let bytes_read = test_file.read(&mut buf).unwrap();
        assert_eq!(Into::<u32>::into(bytes_read), 601u32);
        for i in 0..601 {
            assert_eq!(buf[i], truth[i], "Byte {} didn't match", i);
        }
    }
}

// Test reading a file that's 2.x sectors big into a bigger buffer
#[test]
fn test_two_plus_sector_large_buffer_read() {
    let mut files = HashMap::new();
    let test_file = PathBuf::from("tests/two_plus_sectors.txt");
    files.insert(PathBuf::from("/test.txt"), Object::File(test_file.clone()));
    let image_num = 30;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);
    let mut truth = Vec::new();
    File::open(test_file).unwrap().read_to_end(&mut truth).unwrap();
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();
    let root = fs.root().unwrap();
    {
        let mut test_file = root.open_file("test.txt").unwrap().unwrap();
        let mut buf: [u8; 4000] = unsafe { mem::uninitialized() };
        let bytes_read = test_file.read(&mut buf).unwrap();
        assert_eq!(Into::<u32>::into(bytes_read), 1201u32);
        for i in 0..1201 {
            assert_eq!(buf[i], truth[i], "Byte {} didn't match", i);
        }
    }
}

// Test reading a large file
#[test]
fn test_large_file() {
    // Create a large test file
    let test_file_path = "tests/large_test_file";
    Command::new("dd")
        .args(&["if=/dev/urandom", "of=tests/large_test_file", "bs=10M", "count=1"])
        .output()
        .expect("Calling dd failed. Make sure it's properly installed.");

    // Package that single file into the root directory of a FAT32 partition
    let mut files = HashMap::new();
    let test_file = PathBuf::from(test_file_path);
    files.insert(PathBuf::from("/test.txt"), Object::File(test_file.clone()));
    let image_num = 31;
    let image_name = format!("test{}.img", image_num);
    generate_image(&image_name, files);

    // Read the file from the partition a sector at a time
    let mut truth = Vec::new();
    File::open(test_file).unwrap().read_to_end(&mut truth).unwrap();
    let fs = FatFileSystem::<FileDevice>::mount(image_num).unwrap();
    let root = fs.root().unwrap();
    {
        let mut test_file = root.open_file("test.txt").unwrap().unwrap();
        let mut buf: [u8; 4096] = unsafe { mem::uninitialized() };
        while !test_file.at_end() {
            test_file.read(&mut buf).unwrap();
        }
    }
}
